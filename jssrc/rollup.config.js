export default {
    entry: 'src/app.js',
    indent: '    ',
    // sourceMap: true,
    targets: [
        {
            format: 'umd',
            moduleName: 'LETAK',
            dest: '../src/main/resources/static/js/letak.js'
        },
    ]
}
