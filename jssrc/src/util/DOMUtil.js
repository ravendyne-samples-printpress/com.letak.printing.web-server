/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

/**
 * CreateDOMElement( data, '.class / text' )
 *  - find template with '.class' and get its html() or use 'text' directly
 *  - fill in template from data object and create $DOMElement
 *  - return $DOMElement
 *
 * CreateDOMElement( data, '.class / text', list_items, '.insert_class' )
 *  - find template with '.class' and get its html() or use 'text' directly
 *  - fill in template from data object and create $DOMElement
 *  - find '.insert_class' point in $DOMElement
 *  - empty() insert point and then append all list_items assuming they are $() DOMElements
 *
 * CreateDOMElement( data, '.class / text', list_items, '.insert_class', 'item .class / text' )
 *  - find template with '.class' and get its html() or use 'text' directly
 *  - fill in template from data object and create $DOMElement
 *  - for each item in list_items
 *      - item = CreateDOMElement( item, 'item .class / text' )
 *  - find '.insert_class' point in $DOMElement
 *  - empty() insert point and then append all list_items ( they are $() DOMElements now created in step above )
 */
const CreateDOMElement = function (data, data_template, list_items, list_item_insert_point_id, list_item_template) {

    // find list container template and get its content
    var dataContainerText = data_template
    if (data_template.startsWith('.')) {
        dataContainerText = $(data_template).html()
    }
    // substitute data object values
    dataContainerText = LETAK.FormatStringWithObject(dataContainerText, data)
    // create DOM element
    const $dataContainer = $(dataContainerText)


    if( list_items !== undefined && list_item_insert_point_id !== undefined ) {

        // find list insert point node
        // in case $dataContainer jquery object contains more than one element
        // find() will not find list_item_insert_point_id node unless it's a child of the first element
        // find() only searches through children so we wrap span around $dataContainer here
        // just to make sure find() finds our element
        const $listInsertPoint = $('<span></span>').append($dataContainer).find(list_item_insert_point_id)
        // clear it
        $listInsertPoint.empty()

        // iterate through items array
        // apply list item template to each (if available)
        // and append it to list insertion point
        list_items.forEach( item => {

            var $listItem = item

            if (list_item_template !== undefined) {

                $listItem = CreateDOMElement( item, list_item_template )
            }

            // append to insertion point node
            $listInsertPoint.append($listItem)
        })
    }

    // return data DOM node
    return $dataContainer
}

/**
 * Creates a list of DOMElements from dataArray using item_template which can
 * either be a string containing HTML code or an existing DOM element id in current page.
 * 
 * Once created, all DOMElements are appended to an existing DOM element with id given
 * in dom_parent_id. The parent element is empty()-ed before DOMElements are appended.
 * 
 * @param {array} dataArray 
 * @param {string} dom_parent_id 
 * @param {string} item_template 
 */
const CreateAndAppendList = function( dataArray, dom_parent_id, item_template ) {

    // creates a list of DOM elements from dataArray
    // and attaches all of them to span[class=.template.here] element
    const $productList = CreateDOMElement({}, '<span class="template.here"></span>', dataArray, '.template\\.here', item_template)

    const $tableBodyContainer = $(dom_parent_id)
    $tableBodyContainer.empty()
    // we just need children, we don't need span[class=.template.here] element
    $tableBodyContainer.append($productList.children())

}

export { CreateDOMElement, CreateAndAppendList }
