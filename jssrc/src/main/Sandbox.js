
function Sandbox( param )
{
    //
    // Case when Sandbox is called as a function
    //
    if( ! ( this instanceof Sandbox ) )
    {
        return new Sandbox( param )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _prop = param

    //
    // PRIVATE FUNCTIONS
    //
    const privateFunction = function()
    {

    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        // private properties getters/setters
        // so we don't have to, i.e. check if assigned values are numbers
        // or to try/catch when working with private props in every other method
        // prop:
        // {
        //     get: function() { return _prop },
        //     set: function( value ) { if( Number.isFinite( value ) ) _prop = value },
        // },
    })

    //
    // constructor code
    //
    // if( Number.isFinite( param ) )
    // {
    //     this.prop = param
    // }
    // if( param instanceof Sandbox )
    // {
    //     this.prop = param.x
    // }

    //
    // initialize private property accessors here
    //
    this.baseClassProp = 'fubar'
}

//
// PUBLIC CLASS METHODS
//
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Using_propertiesObject_argument_with_Object.create()
Object.assign( Sandbox.prototype,
{

    propBar:
    {
        // property descriptor
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty#Description
        configurable: false,
        get: function() { return 10 },
        set: function(value) { console.log('Setter ', value) }
    },
    toString: function()
    {
    },
} )

export { Sandbox }
