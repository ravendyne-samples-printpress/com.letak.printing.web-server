/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { CreateDOMElement, CreateAndAppendList } from '../util/lib.js'
import { Remote } from './Remote.js'

function PrintPress( param )
{
    //
    // Case when PrintPress is called as a function
    //
    if( ! ( this instanceof PrintPress ) )
    {
        return new PrintPress( param )
    }
}

//
// PUBLIC CLASS METHODS
//
Object.assign( PrintPress.prototype,
{
    listProducts: function( uuid, dom_table_id, template_table_row_id ) {

        const dfd = $.Deferred()

        Remote().getListOfProducts( uuid )
        .done(function( result )
        {
            result.sort((l,r) => l.displayOrder - r.displayOrder)
            result.forEach(element => {
                element.productAttributes.sort((l, r) => l.displayOrder - r.displayOrder)
                element.productAttributes.forEach(element => {
                    element.productAttributeValues.sort((l, r) => l.displayOrder - r.displayOrder)
                });
            });
            CreateAndAppendList(result, dom_table_id, template_table_row_id)
            dfd.resolve(result)
        })
        .fail(function( error )
        {
            console.error('>>>>>>>>>> error ', error);
            dfd.reject()
        })

        return dfd.promise()
    },

    list: function( dom_table_id, template_table_row_id )
    {
        const dfd = $.Deferred()

        Remote().getListOfPrintPress()
        .done(function( result )
        {
            CreateAndAppendList(result, dom_table_id, template_table_row_id)
            dfd.resolve(result)
        })
        .fail(function( error )
        {
            console.error('>>>>>>>>>> error ', error);
            dfd.reject()
        })

        return dfd.promise()
    },

    toString: function()
    {
    },
} )

export { PrintPress }
