/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

function Remote(param) {
    //
    // Case when Remote is called as a function
    //
    if (!(this instanceof Remote)) {
        return new Remote(param)
    }
}

//
// PUBLIC CLASS METHODS
//
Object.assign(Remote.prototype,
{
    getListOfProducts: function (uuid) {
        return $.ajax({
            type: "GET",
            url: `/api/product-service/products/${uuid}`,
            dataType: "json",
            contentType: "application/json",
        })
    },

    getListOfPrintPress: function () {
        return $.ajax({
            type: "GET",
            url: "/api/admin/printers",
            dataType: "json",
            contentType: "application/json",
        })
    },

    getQuote: function (data) {
        return $.ajax({
            type: "POST",
            url: "/api/quote/getquote",
            dataType: "json",
            data: JSON.stringify(data),
            contentType: "application/json",
        })
    },

    toString: function () {
    },
})

export { Remote }
