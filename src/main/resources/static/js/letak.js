(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.LETAK = global.LETAK || {})));
}(this, (function (exports) { 'use strict';

    // Polyfills

    if ( Number.EPSILON === undefined ) {

    	Number.EPSILON = Math.pow( 2, - 52 );

    }

    if ( Number.isInteger === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger

    	Number.isInteger = function ( value ) {

    		return typeof value === 'number' && isFinite( value ) && Math.floor( value ) === value;

    	};

    }

    //

    if ( Math.sign === undefined ) {

    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sign

    	Math.sign = function ( x ) {

    		return ( x < 0 ) ? - 1 : ( x > 0 ) ? 1 : + x;

    	};

    }

    if ( Function.prototype.name === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/name

    	Object.defineProperty( Function.prototype, 'name', {

    		get: function () {

    			return this.toString().match( /^\s*function\s*([^\(\s]*)/ )[ 1 ];

    		}

    	} );

    }

    if ( Object.assign === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign

    	( function () {

    		Object.assign = function ( target ) {

    			'use strict';

    			if ( target === undefined || target === null ) {

    				throw new TypeError( 'Cannot convert undefined or null to object' );

    			}

    			var output = Object( target );

    			for ( var index = 1; index < arguments.length; index ++ ) {

    				var source = arguments[ index ];

    				if ( source !== undefined && source !== null ) {

    					for ( var nextKey in source ) {

    						if ( Object.prototype.hasOwnProperty.call( source, nextKey ) ) {

    							output[ nextKey ] = source[ nextKey ];

    						}

    					}

    				}

    			}

    			return output;

    		};

    	} )();

    }

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * CreateDOMElement( data, '.class / text' )
     *  - find template with '.class' and get its html() or use 'text' directly
     *  - fill in template from data object and create $DOMElement
     *  - return $DOMElement
     *
     * CreateDOMElement( data, '.class / text', list_items, '.insert_class' )
     *  - find template with '.class' and get its html() or use 'text' directly
     *  - fill in template from data object and create $DOMElement
     *  - find '.insert_class' point in $DOMElement
     *  - empty() insert point and then append all list_items assuming they are $() DOMElements
     *
     * CreateDOMElement( data, '.class / text', list_items, '.insert_class', 'item .class / text' )
     *  - find template with '.class' and get its html() or use 'text' directly
     *  - fill in template from data object and create $DOMElement
     *  - for each item in list_items
     *      - item = CreateDOMElement( item, 'item .class / text' )
     *  - find '.insert_class' point in $DOMElement
     *  - empty() insert point and then append all list_items ( they are $() DOMElements now created in step above )
     */
    const CreateDOMElement = function (data, data_template, list_items, list_item_insert_point_id, list_item_template) {

        // find list container template and get its content
        var dataContainerText = data_template;
        if (data_template.startsWith('.')) {
            dataContainerText = $(data_template).html();
        }
        // substitute data object values
        dataContainerText = LETAK.FormatStringWithObject(dataContainerText, data);
        // create DOM element
        const $dataContainer = $(dataContainerText);


        if( list_items !== undefined && list_item_insert_point_id !== undefined ) {

            // find list insert point node
            // in case $dataContainer jquery object contains more than one element
            // find() will not find list_item_insert_point_id node unless it's a child of the first element
            // find() only searches through children so we wrap span around $dataContainer here
            // just to make sure find() finds our element
            const $listInsertPoint = $('<span></span>').append($dataContainer).find(list_item_insert_point_id);
            // clear it
            $listInsertPoint.empty();

            // iterate through items array
            // apply list item template to each (if available)
            // and append it to list insertion point
            list_items.forEach( item => {

                var $listItem = item;

                if (list_item_template !== undefined) {

                    $listItem = CreateDOMElement( item, list_item_template );
                }

                // append to insertion point node
                $listInsertPoint.append($listItem);
            });
        }

        // return data DOM node
        return $dataContainer
    };

    /**
     * Creates a list of DOMElements from dataArray using item_template which can
     * either be a string containing HTML code or an existing DOM element id in current page.
     * 
     * Once created, all DOMElements are appended to an existing DOM element with id given
     * in dom_parent_id. The parent element is empty()-ed before DOMElements are appended.
     * 
     * @param {array} dataArray 
     * @param {string} dom_parent_id 
     * @param {string} item_template 
     */
    const CreateAndAppendList = function( dataArray, dom_parent_id, item_template ) {

        // creates a list of DOM elements from dataArray
        // and attaches all of them to span[class=.template.here] element
        const $productList = CreateDOMElement({}, '<span class="template.here"></span>', dataArray, '.template\\.here', item_template);

        const $tableBodyContainer = $(dom_parent_id);
        $tableBodyContainer.empty();
        // we just need children, we don't need span[class=.template.here] element
        $tableBodyContainer.append($productList.children());

    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    // https://tools.ietf.org/html/rfc4122
    // http://www.boost.org/doc/libs/1_47_0/boost/uuid/uuid.hpp
    // https://en.wikipedia.org/wiki/Universally_unique_identifier
    const GUID = function()
    {
        const quad = function( zeroMask = 0xFFFF, oneMask = 0x0000 )
        {
            // Get random value between 0x10000 and 0x20000.
            // This will ensure that we have leading zeroes later on
            // when we do conversion to hex string
            var randomValue = Math.floor( ( 1 + Math.random() ) * 0x10000 );
            randomValue = randomValue & ( 0xF0000 | zeroMask ) | oneMask;

            return ''+
                randomValue
                // convert to hex string ( base == 16 ),
                // this string will be 5 characters long
                // since generated value is between 0x10000 and 0x20000.
                .toString( 16 )
                // strip leading character so we are left
                // with 4-character string
                .substring( 1 )
        };

        return '' +
            // time_low
            quad() + quad() + '-' +
            // time_mid
            quad() + '-' +
            // time_hi
            // bits 12-15 to 4-bit verion (v4)
            quad( 0x0FFF, 0x4000 ) + '-' +
            // clock_seq
            // bits 6-7 to 01
            quad( 0x3FFF, 0x8000 ) + '-' +
            // node
            quad() + quad() + quad()
    };


    // http://stackoverflow.com/a/5341855
    // usage:
    // FormatStringWithObject("i can speak {language} since i was {age}",{language:'javascript',age:10})
    // FormatStringWithObject("i can speak {0} since i was {1}",'javascript',10})
    const FormatStringWithObject = function (str, col)
    {
        col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

        return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function ( m, n ) {
            if (m == "{{") { return "{"; }
            if (m == "}}") { return "}"; }

            return col[n]
        })
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    function Remote(param) {
        //
        // Case when Remote is called as a function
        //
        if (!(this instanceof Remote)) {
            return new Remote(param)
        }
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Remote.prototype,
    {
        getListOfProducts: function (uuid) {
            return $.ajax({
                type: "GET",
                url: `/api/product-service/products/${uuid}`,
                dataType: "json",
                contentType: "application/json",
            })
        },

        getListOfPrintPress: function () {
            return $.ajax({
                type: "GET",
                url: "/api/admin/printers",
                dataType: "json",
                contentType: "application/json",
            })
        },

        getQuote: function (data) {
            return $.ajax({
                type: "POST",
                url: "/api/quote/getquote",
                dataType: "json",
                data: JSON.stringify(data),
                contentType: "application/json",
            })
        },

        toString: function () {
        },
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    function PrintPress( param )
    {
        //
        // Case when PrintPress is called as a function
        //
        if( ! ( this instanceof PrintPress ) )
        {
            return new PrintPress( param )
        }
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign( PrintPress.prototype,
    {
        listProducts: function( uuid, dom_table_id, template_table_row_id ) {

            const dfd = $.Deferred();

            Remote().getListOfProducts( uuid )
            .done(function( result )
            {
                result.sort((l,r) => l.displayOrder - r.displayOrder);
                result.forEach(element => {
                    element.productAttributes.sort((l, r) => l.displayOrder - r.displayOrder);
                    element.productAttributes.forEach(element => {
                        element.productAttributeValues.sort((l, r) => l.displayOrder - r.displayOrder);
                    });
                });
                CreateAndAppendList(result, dom_table_id, template_table_row_id);
                dfd.resolve(result);
            })
            .fail(function( error )
            {
                console.error('>>>>>>>>>> error ', error);
                dfd.reject();
            });

            return dfd.promise()
        },

        list: function( dom_table_id, template_table_row_id )
        {
            const dfd = $.Deferred();

            Remote().getListOfPrintPress()
            .done(function( result )
            {
                CreateAndAppendList(result, dom_table_id, template_table_row_id);
                dfd.resolve(result);
            })
            .fail(function( error )
            {
                console.error('>>>>>>>>>> error ', error);
                dfd.reject();
            });

            return dfd.promise()
        },

        toString: function()
        {
        },
    } );

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    var REVISION = '1dev';

    exports.PrintPress = PrintPress;
    exports.Remote = Remote;
    exports.CreateDOMElement = CreateDOMElement;
    exports.CreateAndAppendList = CreateAndAppendList;
    exports.GUID = GUID;
    exports.FormatStringWithObject = FormatStringWithObject;
    exports.REVISION = REVISION;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
