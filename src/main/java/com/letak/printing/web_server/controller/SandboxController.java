package com.letak.printing.web_server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/sandbox")
public class SandboxController {
    
    @RequestMapping("")
    public String root(Model model)
    {
        return "sandbox";
    }

}
