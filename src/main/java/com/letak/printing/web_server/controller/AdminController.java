package com.letak.printing.web_server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {
    
    @RequestMapping("")
    public String root(Model model)
    {
        return "redirect:/admin/dashboard";
    }
    
    @RequestMapping("dashboard")
    public String dashboard(Model model)
    {
        model.addAttribute("pageTitle", "PrintPress Admin");

        return "dashboard";
    }
    
    @RequestMapping("printpresslist")
    public String printpressList(Model model)
    {
        model.addAttribute("pageTitle", "PrintPress Admin - Printpress List");

        return "printpresslist";
    }

}
