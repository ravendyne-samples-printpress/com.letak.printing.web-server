package com.letak.printing.web_server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

	@RequestMapping("/")
	public String index(Model model) {

		model.addAttribute("pageTitle", "Print-Press Web Frontend");

		return "index";
	}
    
    @RequestMapping("examples/{page}")
    public String examples(@PathVariable("page") String examplesPage, Model model) {

        if( examplesPage == null ) examplesPage = "dashboard";

        return "examples/" + examplesPage;
    }
    
    @RequestMapping("printer/{uuid}")
    public String printer(@PathVariable("uuid") String uuid, Model model) {

        model.addAttribute("pageTitle", "e-commerce page");

        if( uuid == null ) uuid = "default";

        return "printers/" + uuid + "/index";
    }

}
